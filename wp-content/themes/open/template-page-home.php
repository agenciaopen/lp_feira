<?php
/**
*
* Template Name: Home
*
*/

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>


<?php 
    if (wp_is_mobile()): 
        $image = get_field( 'a_hora_de_comprar_e_agora_mobile' );
    else : 
        $image =  get_field( 'a_hora_de_comprar_e_agora_imagem_desktop' ); 
    endif;
?>


<section class="main" style="background-image: url('<?php echo $image;?>');" id="ofertas">
    <div class="container-fluid h-100">
        <div class="row align-items-center justify-content-end">
            <div class="col-md-5 col-lg-5 col-xl-3  d-none d-lg-block">
                <div class="col-md-12 box">
                    <h3><?php the_field( 'titulo_formulario' ); ?></h3>
                    <p><?php the_field( 'descricao_formulario' ); ?></p>
                    <?php the_field( 'formulario' ); ?>
                </div>
            </div>
        </div>
    </div>
</section><!-- /.main -->

<section class="countdown" >
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-12 text-center">
                <h2><?php if ($result == 1) : ?>Falta <?php else :?>Falta <?php endif;?> <span>0
            <?php 
                date_default_timezone_set('America/Sao_Paulo');
                //echo date("D M j G:i:s T Y");

                $date_now =  date("d");
                $final_date = get_field( 'data_final' );
                $result = ($final_date - $date_now);
            ?>
           
           
            
            <?php echo $result; ?>
            </span> <?php if ($result == 1) : ?> dia <?php else :?> dias <?php endif;?></h2>
            </div>
        </div>
    </div>
</section><!-- /.countdown -->

<section class="conquer" id="conquiste">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-7">
                <h1><?php the_field( 'titulo_conquiste' ); ?></h1>
                <?php the_field( 'descricao_conquieste' ); ?>
                <a href="#ofertas" class="d-none d-lg-block">
                    <div class="btn btn_first">
                        <?php the_field( 'texto_do_botao' ); ?>
                    </div>
                </a>
            </div>
            <div class="col-md-5 d-none d-lg-block text-center">
                <img src='<?php the_field( 'imagem_conquiste' ); ?>' class='img-fluid' alt='<?php the_field( 'titulo_conquiste' ); ?>' title='<?php the_field( 'titulo_conquiste' ); ?>' lazy='loading'>
            </div>
        </div>
    </div>
</section><!-- /.conquer -->


<section class="now" id="a-hora-de-comprar-agora">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-12 text-center">
                <h2><?php the_field( 'titulo_a_hora_de_comprar_e_agora' ); ?></h2>
                <p><?php the_field( 'descricao_a_hora_de_comprar_e_agora' ); ?></p>
            </div>
            <div class="col-md-12 carousel_now">
                <?php if ( have_rows( 'cadastro_de_itens' ) ) : ?>
                    <?php while ( have_rows( 'cadastro_de_itens' ) ) : the_row(); ?>
                        <div class="item">
                            <div class="col-md-12 text-center px-md-1">
                                <?php if ( get_sub_field( 'icone' ) ) : ?>
                                    <img src='<?php the_sub_field( 'icone' ); ?>' class='img-fluid mx-auto d-block' alt='<?php the_sub_field( 'titulo' ); ?>' title='<?php the_sub_field( 'titulo' ); ?>' lazy='loading' width="100">
                                <?php endif ?>
                                <h3><?php the_sub_field( 'titulo' ); ?></h3>
                                <p><?php the_sub_field( 'descricao' ); ?></p>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
            <div class="col-md-12 text-center">
                <a href="#ofertas" rel="" class="d-none d-lg-block">
                    <div class="btn btn_first">
                        <?php the_field( 'texto_do_botao_a_hora_de_comprar_e_agora' ); ?>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section><!-- /.now -->


<section class="about" id="o-que-e-a-feira">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="col-md-5 d-none d-lg-block text-center">
                <img src='<?php the_field( 'imagem_o_que_e_a_feira_de_imoveis_online' ); ?>' class='img-fluid' alt='<?php the_field( 'titulo_o_que_e_a_feira_de_imoveis_online' ); ?>' title='<?php the_field( 'titulo_o_que_e_a_feira_de_imoveis_online' ); ?>' lazy='loading'>
            </div>
            <div class="col-md-7">
                <h2 class="text-center"><?php the_field( 'titulo_o_que_e_a_feira_de_imoveis_online' ); ?></h2>
                <p><?php the_field( 'descricao_o_que_e_a_feira_de_imoveis_online' ); ?></p>
            </div>
        </div>
    </div>
</section><!-- /.about -->
<?php 
/*
<section class="new_home" id="sorteio">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-6">
                <h2><?php the_field( 'titulo_casa_nova_e_carro_na_garagem_' ); ?></h2>
                <img src='<?php the_field( 'imagem_casa_nova_e_carro_na_garagem_' ); ?>'  alt='<?php the_field( 'titulo_casa_nova_e_carro_na_garagem_' ); ?>' title='<?php the_field( 'titulo_casa_nova_e_carro_na_garagem_' ); ?>' lazy='loading' class="img-fluid d-lg-none">
                <p><?php the_field( 'descricao_casa_nova_e_carro_na_garagem_' ); ?></p>
                <a href="#ofertas" rel="" class="d-none d-lg-block">
                    <div class="btn btn_first">
                        <?php the_field( 'botao_casa_nova_e_carro_na_garagem_' ); ?>
                    </div>
                </a>                
            </div>
            <div class="col-md-6 d-none d-lg-block text-center">
                <img src='<?php the_field( 'imagem_casa_nova_e_carro_na_garagem_' ); ?>' class='img-fluid' alt='<?php the_field( 'titulo_casa_nova_e_carro_na_garagem_' ); ?>' title='<?php the_field( 'titulo_casa_nova_e_carro_na_garagem_' ); ?>' lazy='loading' class="d-lg-none">
            </div>
        </div>
    </div>
</section><!-- /.new_home -->
*/ 
?>
<section class="live pt-5" id="live">
    <div class="container h-100">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-6">
                <div class="col-md-12 p-0 order-xs-2 d-none d-md-block">
                    <h2><?php the_field( 'titulo_live_show_com__mumuzinho_e_marcelo_marrom__' ); ?></h2>
                </div>
                <div class="col-md-12 p-0 order-xs-1">
                    <img src='<?php the_field( 'imagem_live_show_com__mumuzinho_e_marcelo_marrom__' ); ?>'  alt='<?php the_field( 'titulo_live_show_com__mumuzinho_e_marcelo_marrom__' ); ?>' title='<?php the_field( 'titulo_live_show_com__mumuzinho_e_marcelo_marrom__' ); ?>' lazy='loading' class="img-fluid">
                </div>
                <div class="col-md-12 p-0 d-md-none">
                    <h2><?php the_field( 'titulo_live_show_com__mumuzinho_e_marcelo_marrom__' ); ?></h2>
                </div>
            </div>
            <div class="col-md-6">
                <h3 class="d-none d-md-block"><?php the_field( 'subtitulo_marque_na_agenda' ); ?></h3>
                <p>Dia: <b><?php the_field( 'dia_' ); ?></b></p>
                <p>Hora: <b><?php the_field( 'hora' ); ?></b></p>
                <p><?php the_field( 'descricao_marque_na_agenda', false, false ); ?></p>
                <a href="#ofertas" rel="" class="d-none d-lg-block">
                    <div class="btn btn_first">
                        <?php the_field( 'botao_marque_na_agenda' ); ?>
                    </div>
                </a>
            </div>
        </div>
    </div>
</section><!-- /.live -->
<section class="ofertas_call d-lg-none">
    <a href="#ofertas_m">
        <div class="btn btn_first">
            quero receber ofertas da feira
        </div>
    </a>
</section>
<section class="ofertas d-lg-none" id="ofertas_m">
    <div class="container-fluid p-0">
        <div class="col-md-12 box">
            <h3><?php the_field( 'titulo_formulario' ); ?></h3>
            <p><?php the_field( 'descricao_formulario' ); ?></p>
            <?php the_field( 'formulario' ); ?>
        </div>
    </div>
</section>

<section class="partners" id="nossas-parceiras">
    <div class="container h-100">
        <div class="row h-100 align-items-start justify-content-center">
            <div class="col-md-8 text-center">
                <h2><?php the_field( 'nossas_parcerias_titulo', $page_ID); ?></h2>
                <p><?php the_field( 'descricao_nossas_parcerias', $page_ID , false, false); ?></p>
            </div>
            <div class="col-md-8 logos" id="realizacao">
                <h3>Realização</h3>
                <?php if ( have_rows( 'realizacao', $page_ID ) ) : ?>
                    <div class="row m-0 h-100">
                        <?php while ( have_rows( 'realizacao', $page_ID ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'logo_' ) ) : ?>
                                <div class="col-4">
                                    <img src='<?php the_sub_field( 'logo_' ); ?>' class='img-fluid' alt='<?php the_sub_field( 'nome' ); ?>' title='<?php the_sub_field( 'nome' ); ?>' lazy='loading'>
                                </div>
                            <?php endif ?>
                        <?php endwhile; ?>
                    </div>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div><!-- /#realizacao -->
            <div class="col-md-4 logos" id="banco">
                <h3>Banco Oficial da Feira</h3>
                <?php if ( have_rows( 'banco_oficial_da_feira', $page_ID ) ) : ?>
                    <div class="row m-0 h-100">
                        <div class="col-12">
                            <?php $count= 0; while ( have_rows( 'banco_oficial_da_feira', $page_ID ) ) : the_row(); ?>
                                <?php if ( get_sub_field( 'imagem' ) ) : ?>
                                    <img src='<?php the_sub_field( 'imagem' ); ?>' class='img-fluid' alt='<?php the_sub_field( 'nome' ); ?>' title='<?php the_sub_field( 'nome' ); ?>' lazy='loading'>
                                <?php endif ?>
                            <?php $count++; endwhile; ?>
                        </div>
                    </div>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div><!-- /#banco -->
            <div class="col-8 logos" id="patrocinio">
                <h3>Patrocínio</h3>
                <?php if ( have_rows( 'patrocinio', $page_ID ) ) : ?>
                    <div class="row m-0 h-100">
                        <?php while ( have_rows( 'patrocinio', $page_ID ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'imagem' ) ) : ?>
                                <div class="col-4">
                                    <img src='<?php the_sub_field( 'imagem' ); ?>' class='img-fluid' alt='<?php the_sub_field( 'nome' ); ?>' title='<?php the_sub_field( 'nome' ); ?>' lazy='loading'>
                                </div>
                            <?php endif ?>
                        <?php endwhile; ?>
                    </div>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div><!-- /#patrocinio-->
            <div class="col-4 logos" id="parceiro">
                <h3>Parceiro</h3>
                <?php if ( have_rows( 'parceiro', $page_ID ) ) : ?>
                    <div class="row m-0 h-100">
                        <?php while ( have_rows( 'parceiro', $page_ID ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'imagem' ) ) : ?>
                                <div class="col-12">
                                    <img src='<?php the_sub_field( 'imagem' ); ?>' class='img-fluid' alt='<?php the_sub_field( 'nome' ); ?>' title='<?php the_sub_field( 'nome' ); ?>' lazy='loading'>
                                </div>
                            <?php endif ?>
                        <?php endwhile; ?>
                    </div>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div><!-- /#parceiro-->
            <div class="col-md-5 logos" id="apoiadores">
                <h3>Apoiadores</h3>
                <?php if ( have_rows( 'apoiador', $page_ID ) ) : ?>
                    <div class="row m-0 h-100">
                        <?php while ( have_rows( 'apoiador', $page_ID ) ) : the_row(); ?>
                            <?php if ( get_sub_field( 'imagem' ) ) : ?>
                                <div class="col-6 col-md-6">
                                    <img src='<?php the_sub_field( 'imagem' ); ?>' class='img-fluid' alt='<?php the_sub_field( 'nome' ); ?>' title='<?php the_sub_field( 'nome' ); ?>' lazy='loading'>
                                </div>
                            <?php endif ?>
                        <?php endwhile; ?>
                    </div>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
            <div class="col-md-7"></div>
        </div>
    </div>
</section><!-- /.partners -->



<!-- Modal -->
<div class="modal fade" id="success_ofertas" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
            
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">
            <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                <rect width="32" height="32" rx="16" fill="#C30967"/>
                <path d="M23.6451 8.36708C23.1718 7.89382 22.4073 7.89382 21.934 8.36708L16 14.289L10.066 8.35495C9.59272 7.88168 8.82821 7.88168 8.35495 8.35495C7.88168 8.82821 7.88168 9.59272 8.35495 10.066L14.289 16L8.35495 21.934C7.88168 22.4073 7.88168 23.1718 8.35495 23.6451C8.82821 24.1183 9.59272 24.1183 10.066 23.6451L16 17.711L21.934 23.6451C22.4073 24.1183 23.1718 24.1183 23.6451 23.6451C24.1183 23.1718 24.1183 22.4073 23.6451 21.934L17.711 16L23.6451 10.066C24.1062 9.60485 24.1062 8.82821 23.6451 8.36708Z" fill="#F2F2F2"/>
            </svg>
          </span>
        </button>
      </div>
      <div class="modal-body">
          <div class="col-md-12 text-center">
            <svg width="100" height="100" viewBox="0 0 100 100" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <circle cx="50" cy="50" r="50" fill="#05C46A"/>
                    <path fill-rule="evenodd" clip-rule="evenodd" d="M39.6686 63.7697L78.0719 24.1809L83.9162 30.2056L39.6686 75.8191L16.0837 51.5062L21.928 45.4815L39.6686 63.7697Z" fill="white"/>
                </svg>
          </div>
        <h2 class="text-center">Seu cadastro foi realizado com sucesso!</h2>
        <p>Agora, podemos te atualizar sobre tudo que acontecerá na Feira de Imóveis Online e também enviar as melhores ofertas diretamente para o seu e-mail.</p>
        <p>Esperamos que você encontre o imóvel perfeito na nossa Feira.</p>
        <p>Nosso show de ofertas acontece entre os dias 20 e 28 de novembro. Até lá!</p>
      </div>
     
    </div>
  </div>
</div>








<?php get_footer(); ?>