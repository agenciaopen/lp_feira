<?php
/**
*
* 404 pages (not found)
*
*/

get_header();
// get page ID
?>
<?php
global $post;
$page_ID = $post->ID;
// get page ID

if(wp_is_mobile()):
    if ( has_post_thumbnail() ) {
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
    } else {
        $featured_img_url = '/wp-content/themes/open/img/erro404m.png';
    }
else:
    if ( has_post_thumbnail() ) {
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
    } else {
        $featured_img_url = '/wp-content/themes/open/img/erro404.png';
    } 
endif;
?>


<section class="main_inner pt-5" style="background-image:url('');">
    <div class="container-fluid p-0 h-100">
        <div class="row h-100 align-items-end justify-content-center m-0">
            <img src='<?php echo $featured_img_url;?>' class='img-fluid w-100' alt='' title='' lazy='loading'>
        </div>
    </div>
</section><!-- /.main -->
<section class="content pt-3">
<div class="container h-100">
                            <div class="row h-100 align-items-center justify-content-center">
                                <div class="col-md-12">
    <h1>A página que você procura não foi encontrada.  </h1>

<p>Mas na Feira você pode achar seu novo endereço. São imóveis em oferta por todo Brasil com condições exclusivas da Caixa e um deles pode ser seu.  </p>

 

<p>Acesse a página inicial, confira todos os detalhes e cadastre-se para receber as melhores ofertas em seu e-mail </p>

<a href="<?php echo home_url(); ?>">
    <div class="btn btn_first">
    Voltar a página inicial 
    </div>
</a>
</div>
</div>
</div>
</section>

<?php get_footer(); ?>




             