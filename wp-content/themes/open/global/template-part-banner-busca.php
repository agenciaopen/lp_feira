<?php
global $post;
$page_ID = $post->ID;
// get page ID

if(wp_is_mobile()):
    if ( has_post_thumbnail() ) {
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
    } else {
        $featured_img_url = '/wp-content/uploads/2020/09/banner-home.png';
    }
else:
    if ( has_post_thumbnail() ) {
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
    } else {
        $featured_img_url = '/wp-content/uploads/2020/09/banner-home.png';
    } 
endif;
?>
<?php if( get_field('titulo_principal', $page_ID) ): ?>
    <?php $title = get_field('titulo_principal', $page_ID); ?>
<?php else: ?>
    <?php $title = get_the_title(); ?>
<?php endif; ?>

<section class="main" style="background-image:url('<?php echo $featured_img_url;?>');">
    <div class="container h-100">
        <div class="row h-100 align-items-end justify-content-center">
            <div class="col-md-12 text-center">
                <h1><?php the_field( 'descricao_principal', $page_ID, false, false); ?></h1>

                <hr>
                <h2><?php echo $title;?></h2>

                <div class="row m-0 justify-content-center">
                
                    <?php echo do_shortcode('[searchandfilter id="1990"]');?>
                </div>
                
                
            </div>
        </div>
    </div>
    
</section><!-- /.main -->
