<?php 
global $post;
//$page_ID = $post->ID;
// get page ID
$page_ID = get_option('page_on_front');

?>
<section class="aqui-atex">
    <div class="container">
        <div class="row">
            <div class="col-md-9 order-2 order-md-1">
                <div class="atex_carousel">
                <?php 
                        // Custom WP query query
                        // Query Arguments
                        $args_query = array(
                            'post_status' => array('publish'),
                            'posts_per_page' => 9,
                            'post_type' => 'obras',
                            'order' => 'DESC',
                        );

                        // The Query
                        $query = new WP_Query( $args_query );

                        // The Loop
                        if ( $query->have_posts() ) {
                            while ( $query->have_posts() ) {
                                $query->the_post();
                                    // Your custom code ?>
                                    <div class="card">
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                            <div class="card-header">
                                                <?php $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full'); 
                                                echo the_post_thumbnail('full');?>
                                            </div>
                                            <div class="card-content">
                                                <span class="barra"></span>
                                            
                                                <h5><?php the_title(); ?></h5>
                                                <span><?php the_field('localidade');?></span>
                                            </div>
                                        </a>
                                    </div>                    
                                    
                                        
<?php                            }
                        } else {
                            // no posts found

                        }

                        /* Restore original Post Data */
                        wp_reset_postdata();

                        ?>
                    
                    
                </div>
            </div>

            <div class="col-md-3 card-right order-1 order-md-2">
                <h2 class="col-10 col-md-12 p-0"><?php the_field( 'titulo_aqui_tem', $page_ID ); ?></h2>
                <p><?php the_field( 'descricao_aqui_tem', $page_ID ); ?></p>
            </div>
            <div class="col-md-12 order-3">
            <?php $botao_aqui_tem = get_field( 'botao_aqui_tem', $page_ID ); ?>
            <?php if ( $botao_aqui_tem ) : ?>
                <a href="<?php echo esc_url( $botao_aqui_tem['url'] ); ?>" target="<?php echo esc_attr( $botao_aqui_tem['target'] ); ?>">
                    <button class="btn btn_first col-md-3 mt-4 mb-4"><?php echo esc_html( $botao_aqui_tem['title'] ); ?></button>
                </a>
            <?php endif; ?>
            </div>
        </div>
    </div>
</section>