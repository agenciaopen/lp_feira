<section class="avaliacoes">
    <div class="container">
        <div class="row">
            <div class="col-10 col-md-4 card-right">
                <h2><?php the_field( 'titulo_avaliacao_de_quem_usa', 'option' ); ?></h2>
            </div>
            <div class="col-md-8">
                <div class="aval_carousel">
                    <?php if ( have_rows( 'cadastro_de_avaliacoes', 'option' ) ) : ?>
	                    <?php while ( have_rows( 'cadastro_de_avaliacoes', 'option' ) ) : the_row(); ?>
                            <div class="card">
                                <div class="card-header">
                                    <img src="<?php the_sub_field( 'avatar' ); ?>" alt="">
                                    <p><?php the_sub_field( 'depoimento' ); ?></p>
                                </div>
                                <div class="card-content">
                                    
                                    <h5>		<?php the_sub_field( 'nome' ); ?></h5>
                                    <h6><b>		<?php the_sub_field( 'empresa' ); ?></b></h6>
                                </div>
                            </div>
                        <?php endwhile; ?>
                    <?php else : ?>
                        <?php // no rows found ?>
                    <?php endif; ?>
                </div>
            </div> 
        </div>
    </div>
 </section><!-- /.avaliacoes -->