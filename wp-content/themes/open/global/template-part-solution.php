<?php
global $post;
//$page_ID = $post->ID;
// get page ID
$page_ID = get_option('page_on_front');

?>
<section class="constructions">
    <div class="container h-100">
        <div class="row h-100 align-items-start justify-content-between">
            <div class="card-header col-md-4">
                <h2><?php the_field('titulo_obras', $page_ID); ?></h2>
                <p><?php the_field('subtitulo_obras', $page_ID); ?></p>
            </div>
            <div class="col-md-8">
                <ul class="solution-projeto">
                    <?php
                    // Custom WP query query
                    // Query Arguments
                    $args_query = array(
                        'post_status' => array('publish'),
                        'posts_per_page' => 6,
                        'post_type' => 'solucoes',
                        'order' => 'DESC',
                    );

                    // The Query
                    $query = new WP_Query($args_query);

                    // The Loop
                    if ($query->have_posts()) {
                        while ($query->have_posts()) {
                            $query->the_post();
                            // Your custom code 
                    ?>
                            <li class="card_content">
                                <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
                                    <h3>Sistema construtivo para</h3>
                                    <p> <b><?php the_title(''); ?></b></p>
                                    <?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                        if ( $url ) :
                                            $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large');
                                        else :
                                            $url = '/wp-content/uploads/2020/09/5cd46a50e763334d7a4647357cb5f64d.png';
                                        endif;
                                    ?>
                                    <img src="<?php echo $url ?>" class="img-fluid" />
                                </a>
                            </li>


                    <?php                            }
                    } else {
                        // no posts found

                    }

                    /* Restore original Post Data */
                    wp_reset_postdata();

                    ?>


                </ul>
            </div>
        </div>
    </div>
</section><!-- /.constructions -->