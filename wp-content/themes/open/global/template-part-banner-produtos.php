<?php
global $post;
$page_ID = $post->ID;
// get page ID


    $imagem_destacada = get_field( 'imagem_destacada', $page_ID );
    if ( $imagem_destacada ) :
        $featured_img_url = get_field('imagem_destacada'); 
    else :
        $featured_img_url = '/wp-content/uploads/2020/09/banner-home.png';
    endif;

?>
<?php if( get_field('titulo', $page_ID) ): ?>
    <?php $title = get_field('titulo', $page_ID); ?>
<?php else: ?>
    <?php $title = get_the_title(); ?>
<?php endif; ?>

<section class="main" style="background-image:url('<?php echo $featured_img_url;?>');">
    <div class="container h-100">
        <div class="row h-100 align-items-end justify-content-center">
            <div class="col-md-12 col-lg-12">
                <h1 class="font-weight-normal"><?php echo $title;?><br>
                    <b><?php echo the_title();?></b>
                </h1>
                <hr>
                <h2><?php the_field( 'subtitulo', $page_ID, false, false); ?></h2>
                <?php $botao = get_field( 'link_orcamento', $page_ID ); ?>
                <?php if ( $botao ) : ?>
                    <a href="<?php echo esc_url( $botao['url'] ); ?>" target="<?php echo esc_attr( $botao['target'] ); ?>">
                        <button class="btn btn_first mt-4 mx-auto mb-4"><?php echo esc_html( $botao['title'] ); ?></button>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section><!-- /.main -->