<section class="como-funciona">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2><?php the_sub_field( 'como_funciona_titulo' ); ?></h2>
                <p><?php the_sub_field( 'como_funciona_texto' ); ?></p>
            </div>
            <div class="content-video col-md-6">
                <?php 
                    $video = the_sub_field( 'video_como_funciona' );
                ?>

                <iframe id="construct_movie" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="100%" height="auto" type="text/html" src=""></iframe>
                <input id="construct_movieID" type="hidden" value="<?php echo $video; ?>">

                <script>
                    var video_caller = function() {
                    var get_vdId = document.getElementById('construct_movieID').value;
                    var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
                    var set_victim = document.getElementById('ytplayer');

                    if (get_vdId.includes('https://youtu.be/') == true) {
                        var str_t = get_vdId.replace('https://youtu.be/', '');

                        document.getElementById('construct_movie').setAttribute('src', 'https://www.youtube.com/embed/' +
                            str_t + '?rel=0?enablejsapi=1&autoplay=0');

                    } else {
                        var str_t = get_vdId.replace('https://www.youtube.com/watch?v=', '');
                        document.getElementById('construct_movie').setAttribute('src', 'https://www.youtube.com/embed/' +
                            str_t + '?rel=0?enablejsapi=1&autoplay=0');
                        }
                    }

                    try {
                        video_caller();
                    } catch (error) {
                        console.log(error);
                    }
                </script>
            </div>
            <div class="col-md-12">
                <button class="btn btn_first mb-4">Baixar o manual</button>
            </div>
        </div>
    </div>
</section><!--/.como-funciona-->