<section class="tire-duvidas">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Tire suas duvidas</h2><br>
                <p>Ficou com alguma dúvida sobre os produtos Atex?</p><br>
                <p>Entre em contato e fale com nossos especialistas.</p>
            </div>
            <div class="col-md-8 doubts">
                <?php echo do_shortcode( '[contact-form-7 id="282" title="Formulário de Dúvidas"]' ); ?>
            </div>
        </div>
    </div>
</section><!--/.tire-duvidas-->