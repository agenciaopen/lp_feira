<?php
global $post;
$page_ID = $post->ID;
// get page ID

if(wp_is_mobile()):
    if ( has_post_thumbnail() ) {
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
    } else {
        $featured_img_url = '/wp-content/uploads/2020/09/banner-home.png';
    }
else:
    if ( has_post_thumbnail() ) {
        $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
    } else {
        $featured_img_url = '/wp-content/uploads/2020/09/banner-home.png';
    } 
endif;
?>


<section class="main_inner pt-3 pt-md-5" style="background-image:url('');">
    <div class="container-fluid p-0 h-100">
        <div class="row h-100 align-items-end justify-content-center m-0">
            <img src='<?php echo $featured_img_url;?>' class='img-fluid w-100' alt='' title='' lazy='loading'>
        </div>
    </div>
</section><!-- /.main -->