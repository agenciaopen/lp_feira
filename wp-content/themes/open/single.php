<?php
get_header();

?>

			<?php
			if ( have_posts() ) :
				while ( have_posts() ) :
					the_post();
                    ?>
                 <?php 
                 
                 if(wp_is_mobile()):
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'full'); 
                else:
                    $featured_img_url = get_the_post_thumbnail_url(get_the_ID($page_ID),'large'); 
                endif;
                ?>
              
                <?php $title = get_the_title(); ?>
                
                <section class="main post" style="background-image: url('<?php echo $featured_img_url;?>');">
                    <div class="container h-100">
                        <div class="row h-100 align-items-center justify-content-center">
                            <div class="col-md-12 text-center">
                                <h1>
                                    <?php echo $title;?>
                                    <span class="d-block"><?php the_field( 'subtitulop', $page_ID); ?></span>
                                </h1>
                                <div class="date">Publicado por <?php the_author(); ?> em <?php echo get_the_date(); ?></div>
                            </div>
                                
                        </div>
                    </div>
                </section><!-- /.main -->
                 <section class="blog-post">
                     <div class="container">
                         <article id="<?php echo $post->ID; ?>" class="section section-text">
                            <?php the_content();?>
                         </article>
                     </div>
                 </section><!-- /.blog-post -->
                 
<?php
				endwhile;
				else :
					get_template_part( 'template-parts/content', 'none' );
			endif;
				?>


	<?php get_footer(); ?>
