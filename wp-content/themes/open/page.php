<?php get_header(); ?>
   
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( 'global/template-part', 'banner' ); ?>
                    <section class="content pt-3">
                        <div class="container h-100">
                            <div class="row h-100 align-items-center justify-content-center">
                                <div class="col-md-12">
                                    <h1 class="d-none"><?php the_title(); ?></h1>
                                    <p><?php the_content(); ?></p>
                                </div>
                            </div>
                        </div>
                    </section>
                   
                </div>
            <?php endwhile; else: ?>
               
            <?php endif; ?>
   
<?php get_footer(); ?>