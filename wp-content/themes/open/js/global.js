
       var header = $("#nav_main");
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();
            if (scroll >= 50) {
                header.addClass("scrolled");
            } else {
                header.removeClass("scrolled");
            }
        });
        $('#bs-example-navbar-collapse-1').on('show.bs.collapse', function () {
          $(".navbar-collapse").addClass("show");
          $(".navbar-brand").addClass("hide");

      })
        
        $('#bs-example-navbar-collapse-1').on('hide.bs.collapse', function () {
            $(".navbar-collapse").removeClass("show");
            $(".navbar-brand").removeClass("hide");

        })
        $('.nav-link').click(function() {
          $('.navbar-collapse').collapse('hide');
        });
        // $('footer .dropdown-toggle').click(function(){
        //     $('#nav_main').removeClass('nav-open');
        //     $('footer').toggleClass('nav-open');
        // });
        // $("footer .dropdown").addClass("dropup");  
       
        /*==============================================================
        navbar fixed top
        ==============================================================*/
        // Hide Header on on scroll down
        var didScroll;
        var lastScrollTop = 0;
        var delta = 100;
        var navbarHeight = $('#nav_main').outerHeight();

        $(window).scroll(function (event) {
            didScroll = true;
        });

        setInterval(function () {
            if (didScroll) {
                hasScrolled();
                didScroll = false;
            }
        }, 250);

        function hasScrolled() {
            var st = $(this).scrollTop();

            // Make sure they scroll more than delta
            if (Math.abs(lastScrollTop - st) <= delta)
                return;

            // If they scrolled down and are past the navbar, add class .nav-up.
            // This is necessary so you never see what is "behind" the navbar.
            if (st > lastScrollTop && st > navbarHeight) {
                // Scroll Down
                $('.navbar').removeClass('nav-down').addClass('nav-up');
            } else {
                // Scroll Up
                if (st + $(window).height() < $(document).height()) {
                    $('.navbar').removeClass('nav-up').addClass('nav-down');
                }
            }

            lastScrollTop = st;
        }

var behavior = function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
},
    options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior.apply({}, arguments), options);
        }
    };

$('input.tel, input.wpcf7-tel').mask(behavior, options);

$('.carousel_now').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 4,
    arrows: false,
    slidesToScroll: 1,
    infinite: true,
    dots: false,
    cssEase: 'linear',
    adaptiveHeight: true, 
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          dots: true
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          dots: true

        }
      }
    ]
});





  
  // Select all links with hashes
$('a[href*="#"]')
// Remove links that don't actually link to anything
.not('[href="#"]')
.not('[href="#0"]')
.click(function(event) {
  // On-page links
  if (
    location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
    && 
    location.hostname == this.hostname
  ) {
    // Figure out element to scroll to
    var target = $(this.hash);
    target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
    // Does a scroll target exist?
    if (target.length) {
      // Only prevent default if animation is actually gonna happen
      event.preventDefault();
      $('html, body').animate({
        scrollTop: target.offset().top
      }, 1000, function() {
        // Callback after animation
        // Must change focus!
        var $target = $(target);
        $target.focus();
        if ($target.is(":focus")) { // Checking if the target was focused
          return false;
        } else {
          $target.attr('tabindex','-1'); // Adding tabindex for elements not focusable
          $target.focus(); // Set focus again
        };
      });
    }
  }
});

// var top1 = $('#ofertas_m').offset().top;
// var top2 = $('.partners').offset().top;

// $(document).scroll(function() {
//   var scrollPos = $(document).scrollTop();
//   if (scrollPos >= top1 && scrollPos <= top2) {
//     alert('a');
//     $('.ofertas_call').addClass('d-none');
//   }
// });
document.addEventListener( 'wpcf7mailfailed', function( event ) {
  $('#success_ofertas').modal('show');

}, false );

document.addEventListener( 'wpcf7mailsent', function( event ) {
  $('#success_ofertas').modal('show');

}, false );
 
