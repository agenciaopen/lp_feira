WebP Express 0.18.3. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2020-11-13 15:10:50

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.21
- Server software: Apache/2.4.46 (Win64) PHP/7.3.21

Stack converter ignited
Destination folder does not exist. Creating folder: C:\workspace\open\feirao\wp-content\webp-express/webp-images/themes/open\img

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/open/img/banner.png
- destination: C:\workspace\open\feirao\wp-content\webp-express/webp-images/themes/open\img\banner.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/themes/open/img/banner.png
- destination: C:\workspace\open\feirao\wp-content\webp-express/webp-images/themes/open\img\banner.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "85" -m 6 -low_memory "[doc-root]/wp-content/themes/open/img/banner.png" -o "C:\workspace\open\feirao\wp-content\webp-express/webp-images/themes/open\img\banner.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\feirao\wp-content\webp-express/webp-images/themes/open\img\banner.png.webp.lossy.webp'
File:      [doc-root]/wp-content/themes/open/img/banner.png
Dimension: 1440 x 200
Output:    16452 bytes Y-U-V-All-PSNR 43.85 51.44 49.76   45.16 dB
           (0.46 bpp)
block count:  intra4:        799  (68.29%)
              intra16:       371  (31.71%)
              skipped:        17  (1.45%)
bytes used:  header:            174  (1.1%)
             mode-partition:   3254  (19.8%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |    1655 |    3031 |    2209 |     885 |    7780  (47.3%)
 intra16-coeffs:  |     955 |    1154 |     993 |     698 |    3800  (23.1%)
  chroma coeffs:  |     234 |     413 |     427 |     345 |    1419  (8.6%)
    macroblocks:  |      25%|      32%|      24%|      19%|    1170
      quantizer:  |      19 |      14 |      11 |       8 |
   filter level:  |      13 |      27 |       2 |       2 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |    2844 |    4598 |    3629 |    1928 |   12999  (79.0%)

Success
Reduction: 88% (went from 130 kb to 16 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "[doc-root]/wp-content/themes/open/img/banner.png" -o "C:\workspace\open\feirao\wp-content\webp-express/webp-images/themes/open\img\banner.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\feirao\wp-content\webp-express/webp-images/themes/open\img\banner.png.webp.lossless.webp'
File:      [doc-root]/wp-content/themes/open/img/banner.png
Dimension: 1440 x 200
Output:    111750 bytes (3.10 bpp)
Lossless-ARGB compressed size: 111750 bytes
  * Header size: 845 bytes, image data size: 110880
  * Lossless features used: PALETTE
  * Precision Bits: histogram=4 transform=4 cache=3
  * Palette size:   141

Success
Reduction: 16% (went from 130 kb to 109 kb)

Picking lossy
cwebp succeeded :)

Converted image in 1313 ms, reducing file size with 88% (went from 130 kb to 16 kb)
