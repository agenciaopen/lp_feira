WebP Express 0.18.3. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2020-11-13 15:10:50

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.21
- Server software: Apache/2.4.46 (Win64) PHP/7.3.21

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2020/11/CV-logotipo.png
- destination: C:\workspace\open\feirao\wp-content\webp-express/webp-images/uploads/2020\11\CV-logotipo.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2020/11/CV-logotipo.png
- destination: C:\workspace\open\feirao\wp-content\webp-express/webp-images/uploads/2020\11\CV-logotipo.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "85" -m 6 -low_memory "[doc-root]/wp-content/uploads/2020/11/CV-logotipo.png" -o "C:\workspace\open\feirao\wp-content\webp-express/webp-images/uploads/2020\11\CV-logotipo.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\feirao\wp-content\webp-express/webp-images/uploads/2020\11\CV-logotipo.png.webp.lossy.webp'
File:      [doc-root]/wp-content/uploads/2020/11/CV-logotipo.png
Dimension: 1446 x 600 (with alpha)
Output:    18988 bytes Y-U-V-All-PSNR 63.08 55.21 54.32   58.40 dB
           (0.18 bpp)
block count:  intra4:        142  (4.11%)
              intra16:      3316  (95.89%)
              skipped:      2742  (79.29%)
bytes used:  header:            222  (1.2%)
             mode-partition:   2634  (13.9%)
             transparency:    12386 (99.0 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |     354 |       0 |       0 |       0 |     354  (1.9%)
 intra16-coeffs:  |     884 |       0 |       1 |       8 |     893  (4.7%)
  chroma coeffs:  |    1925 |       0 |     210 |     308 |    2443  (12.9%)
    macroblocks:  |      26%|       0%|       0%|      74%|    3458
      quantizer:  |      20 |      20 |      12 |      11 |
   filter level:  |       7 |       5 |       2 |       0 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |    3163 |       0 |     211 |     316 |    3690  (19.4%)
Lossless-alpha compressed size: 12385 bytes
  * Header size: 204 bytes, image data size: 12181
  * Precision Bits: histogram=5 transform=4 cache=0
  * Palette size:   17

Success
Reduction: 47% (went from 35 kb to 19 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "[doc-root]/wp-content/uploads/2020/11/CV-logotipo.png" -o "C:\workspace\open\feirao\wp-content\webp-express/webp-images/uploads/2020\11\CV-logotipo.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\feirao\wp-content\webp-express/webp-images/uploads/2020\11\CV-logotipo.png.webp.lossless.webp'
File:      [doc-root]/wp-content/uploads/2020/11/CV-logotipo.png
Dimension: 1446 x 600
Output:    13664 bytes (0.13 bpp)
Lossless-ARGB compressed size: 13664 bytes
  * Header size: 358 bytes, image data size: 13281
  * Lossless features used: PALETTE
  * Precision Bits: histogram=5 transform=4 cache=0
  * Palette size:   102

Success
Reduction: 62% (went from 35 kb to 13 kb)

Picking lossless
cwebp succeeded :)

Converted image in 1511 ms, reducing file size with 62% (went from 35 kb to 13 kb)
