WebP Express 0.18.3. Conversion triggered using bulk conversion, 2020-11-12 23:09:21

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.4.10
- Server software: Microsoft-IIS/10.0

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: D:\home\site\wwwroot/wp-content/uploads/2020/11/erro404-1536x771.png
- destination: D:\home\site\wwwroot/wp-content/webp-express/webp-images/uploads/2020\11\erro404-1536x771.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 80
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: D:\home\site\wwwroot/wp-content/uploads/2020/11/erro404-1536x771.png
- destination: D:\home\site\wwwroot/wp-content/webp-express/webp-images/uploads/2020\11\erro404-1536x771.png.webp
- alpha-quality: 80
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- D:\home\site\wwwroot\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: D:\home\site\wwwroot\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- D:\home\site\wwwroot\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
D:\home\site\wwwroot\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "80" -m 6 -low_memory "D:\home\site\wwwroot/wp-content/uploads/2020/11/erro404-1536x771.png" -o "D:\home\site\wwwroot/wp-content/webp-express/webp-images/uploads/2020\11\erro404-1536x771.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'D:\home\site\wwwroot/wp-content/webp-express/webp-images/uploads/2020\11\erro404-1536x771.png.webp.lossy.webp'
File:      D:\home\site\wwwroot/wp-content/uploads/2020/11/erro404-1536x771.png
Dimension: 1536 x 771
Output:    13800 bytes Y-U-V-All-PSNR 51.51 51.31 52.28   51.59 dB
           (0.09 bpp)
block count:  intra4:        774  (16.45%)
              intra16:      3930  (83.55%)
              skipped:      1529  (32.50%)
bytes used:  header:            308  (2.2%)
             mode-partition:   4074  (29.5%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |    1874 |       1 |       2 |     137 |    2014  (14.6%)
 intra16-coeffs:  |     181 |      20 |      46 |    1911 |    2158  (15.6%)
  chroma coeffs:  |    3358 |      28 |      24 |    1810 |    5220  (37.8%)
    macroblocks:  |       8%|       1%|       1%|      89%|    4704
      quantizer:  |      20 |      20 |      16 |      13 |
   filter level:  |       9 |       5 |       3 |       2 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |    5413 |      49 |      72 |    3858 |    9392  (68.1%)

Success
Reduction: 98% (went from 577 kb to 13 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- D:\home\site\wwwroot\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: D:\home\site\wwwroot\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- D:\home\site\wwwroot\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
D:\home\site\wwwroot\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "80" -near_lossless 60 -m 6 -low_memory "D:\home\site\wwwroot/wp-content/uploads/2020/11/erro404-1536x771.png" -o "D:\home\site\wwwroot/wp-content/webp-express/webp-images/uploads/2020\11\erro404-1536x771.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'D:\home\site\wwwroot/wp-content/webp-express/webp-images/uploads/2020\11\erro404-1536x771.png.webp.lossless.webp'
File:      D:\home\site\wwwroot/wp-content/uploads/2020/11/erro404-1536x771.png
Dimension: 1536 x 771
Output:    408848 bytes (2.76 bpp)
Lossless-ARGB compressed size: 408848 bytes
  * Header size: 4879 bytes, image data size: 403944
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM
  * Precision Bits: histogram=5 transform=4 cache=10

Success
Reduction: 31% (went from 577 kb to 399 kb)

Picking lossy
cwebp succeeded :)

Converted image in 7110 ms, reducing file size with 98% (went from 577 kb to 13 kb)
