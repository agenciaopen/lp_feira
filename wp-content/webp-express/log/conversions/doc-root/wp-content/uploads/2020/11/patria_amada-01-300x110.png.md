WebP Express 0.18.3. Conversion triggered with the conversion script (wod/webp-on-demand.php), 2020-11-17 20:48:30

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.21
- Server software: Apache/2.4.46 (Win64) PHP/7.3.21

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png
- destination: [doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: [doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png
- destination: [doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "85" -m 6 -low_memory "[doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png" -o "[doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png.webp.lossy.webp'
File:      [doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png
Dimension: 300 x 110 (with alpha)
Output:    6882 bytes Y-U-V-All-PSNR 49.44 43.82 43.64   46.63 dB
           (1.67 bpp)
block count:  intra4:         40  (30.08%)
              intra16:        93  (69.92%)
              skipped:        44  (33.08%)
bytes used:  header:            150  (2.2%)
             mode-partition:    239  (3.5%)
             transparency:     4132 (99.0 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |     602 |       0 |       2 |       0 |     604  (8.8%)
 intra16-coeffs:  |      71 |      10 |      20 |       0 |     101  (1.5%)
  chroma coeffs:  |    1042 |      11 |     549 |       0 |    1602  (23.3%)
    macroblocks:  |      80%|       2%|      18%|       0%|     133
      quantizer:  |      16 |      13 |       8 |       8 |
   filter level:  |       5 |       3 |       0 |       0 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |    1715 |      21 |     571 |       0 |    2307  (33.5%)
Lossless-alpha compressed size: 4131 bytes
  * Header size: 96 bytes, image data size: 4035
  * Precision Bits: histogram=3 transform=3 cache=0
  * Palette size:   128

Success
Reduction: 60% (went from 17 kb to 7 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "[doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png" -o "[doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file '[doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png.webp.lossless.webp'
File:      [doc-root]/wp-content/uploads/2020/11/patria_amada-01-300x110.png
Dimension: 300 x 110
Output:    8928 bytes (2.16 bpp)
Lossless-ARGB compressed size: 8928 bytes
  * Header size: 1260 bytes, image data size: 7643
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM
  * Precision Bits: histogram=2 transform=2 cache=10

Success
Reduction: 48% (went from 17 kb to 9 kb)

Picking lossy
cwebp succeeded :)

Converted image in 1538 ms, reducing file size with 60% (went from 17 kb to 7 kb)
