WebP Express 0.18.3. Conversion triggered using bulk conversion, 2020-11-19 00:08:26

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.21
- Server software: Apache/2.4.46 (Win64) PHP/7.3.21

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png
- destination: C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png.webp
- log-call-arguments: true
- converters: (array of 9 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png
- destination: C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "85" -m 6 -low_memory "C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png" -o "C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png.webp.lossy.webp'
File:      C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png
Dimension: 1080 x 1080 (with alpha)
Output:    10028 bytes Y-U-V-All-PSNR 74.76 55.55 57.85   61.19 dB
           (0.07 bpp)
block count:  intra4:         56  (1.21%)
              intra16:      4568  (98.79%)
              skipped:      4331  (93.66%)
bytes used:  header:            196  (2.0%)
             mode-partition:   2658  (26.5%)
             transparency:     4375 (99.0 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |      60 |       0 |       0 |       0 |      60  (0.6%)
 intra16-coeffs:  |     178 |       0 |       0 |       2 |     180  (1.8%)
  chroma coeffs:  |    2039 |       0 |       0 |     460 |    2499  (24.9%)
    macroblocks:  |       9%|       0%|       0%|      91%|    4624
      quantizer:  |      20 |      20 |      17 |      13 |
   filter level:  |       7 |       5 |       3 |       2 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |    2277 |       0 |       0 |     462 |    2739  (27.3%)
Lossless-alpha compressed size: 4374 bytes
  * Header size: 97 bytes, image data size: 4277
  * Precision Bits: histogram=5 transform=4 cache=0
  * Palette size:   17

Success
Reduction: 37% (went from 16 kb to 10 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -version 2>&1. Result: version: *1.0.3*
Binaries ordered by version number.
- C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe: (version: 1.0.3)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.0.3
Trying to convert by executing the following command:
C:\workspace\open\feirao\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-103-windows-x64.exe -metadata none -q 85 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png" -o "C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png.webp.lossless.webp'
File:      C:\workspace\open\feirao/wp-content/uploads/2020/11/brain_original.png
Dimension: 1080 x 1080
Output:    4566 bytes (0.03 bpp)
Lossless-ARGB compressed size: 4566 bytes
  * Header size: 135 bytes, image data size: 4406
  * Lossless features used: PALETTE
  * Precision Bits: histogram=5 transform=4 cache=0
  * Palette size:   33

Success
Reduction: 71% (went from 16 kb to 4 kb)

Picking lossless
cwebp succeeded :)

Converted image in 1098 ms, reducing file size with 71% (went from 16 kb to 4 kb)
